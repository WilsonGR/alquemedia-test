<?php namespace Alquemedia_PostREST\Components\Extended;
use Alquemedia_PostREST\Components\Models\Model;


/**
 * Class Task
 * @package Alquemedia_PostREST\Components\Extended
 *
 * A Task is a single instance of a data record
 */
class Task {

    /**
     * @var actual model instance
     */
    private $model = null;

    /**
     * @var actual model instance
     */
    private $modelName = null;

    /**
     * Task constructor.
     * @param $modelName
     * @param int $recordId
     */
    public function __construct( $modelName, $recordId = 0, $pageNumber = null ) {

        $this->model = Model($modelName, $pageSize, $pageNumber);

        $this->modelName = $modelName;
    }

    /**
     * @return array of members
     */
    public function asArray(){ 

        $this->model->members['model'] = $this->modelName;

        $this->model->members['myDescription'] = "<p>". $this->model->members['description'] ."</p>";

        return $this->model->members;

    }

}