<?php
/**
 * Created by PhpStorm.
 * User: dgreenberg
 * Date: 10/6/16
 * Time: 10:06 PM
 */

namespace Alquemedia_PostREST\Components\SQL;


use Alquemedia_PostREST\String_Object;

/**
 * Class Select_Paginated_Statement
 * @package Alquemedia_PostREST\Components\SQL
 *
 * A SELECT Statement which expects and includes 
 * record pagination.
 */
class Select_Paginated_Statement extends String_Object {

    /**
     * Select_Paginated_Statement constructor.
     * @param $modelName
     * @param int $recordId
     */
    public function __construct( $modelName, $pageSize = 1, $pageNumber = 1 ) {

        $whereClause = (string) new WHERE_Clause($modelName);

        $this->stringRepresentation = "SELECT * FROM $modelName WHERE $whereClause " 

            ."LIMIT $pageSize " . "OFFSET ". ($pageSize * ($pageNumber - 1));

    }
}